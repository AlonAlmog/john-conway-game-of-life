# John Conway - Game of Life

This GUI application runs a simulation of John Conway's Game of Life.
Looks almost identical to the visualization on the wiki page https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life. 
